/// <reference path="../tsDefinitions/phaser.d.ts" />
/// <reference path="states/Boot.ts" />
/// <reference path="states/Preloader.ts" />
/// <reference path="states/Intro.ts" />
/// <reference path="states/Menu.ts" />
/// <reference path="states/Main.ts" />

module Basegame {
	export class Game extends Phaser.Game {
		
		public static width: number = 400;
		public static height: number = 250;
		
		constructor() {
			super(Game.width, Game.height, Phaser.AUTO, 'game', null);
			
            // Define all states
			this.state.add('Boot', Boot);
			this.state.add('Preloader', Preloader);
			this.state.add('Intro', Intro);
			this.state.add('Menu', Menu);
			this.state.add('Main', Main);
		
			this.state.start('Boot');
		}
	}
}

// when the page has finished loading, create our game
window.onload = () => {
	var game = new Basegame.Game();
}
