# Base Game

## Description

Boiler plate for Phaser's games

## Compile

Install `node` and `npm` from the [Node's home page](https://nodejs.org/) and then the dependencies for this project:

```sh
$ npm install
```

To compile simply do:

```sh
$ gulp build:debug
```

or:

```sh
$ gulp build:deploy
```

to generate an uglified version.


## Running

There's a server supplied to run the game. Launch it with

```sh
$ node server/server.js
```

And browse to [localhost:5858](http://localhost:5858/)

