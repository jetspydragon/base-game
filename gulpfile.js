var gulp = require('gulp');
var ts = require('gulp-typescript');
var uglify = require('gulp-uglify');
var del = require('del');
var rename = require('gulp-rename');

gulp.task('clean', function (cb) {
  del('build/**/*', cb);
});

gulp.task('compile', ['clean'], function () {
  var tsResult = gulp.src('src/**/*.ts')
    .pipe(ts({
        noImplicitAny: true,
        out: 'game.js'
      }));
  return tsResult.js.pipe(gulp.dest('build/js'));
});

gulp.task('build', ['clean', 'compile'], function() {
  gulp.src('node_modules/phaser/build/phaser.min.js')
    .pipe(gulp.dest('build/js'));
  gulp.src([
      'web/**/*',
      '!web/game-*.html'])
    .pipe(gulp.dest('build'));
});

gulp.task('build:debug', ['clean', 'compile','build'], function() {
  gulp.src('web/game-dev.html')
    .pipe(rename("game.html"))
    .pipe(gulp.dest('build'));
});

gulp.task('compress', ['build'], function() {
  return gulp.src('build/js/game.js')
    .pipe(uglify())
    .pipe(rename("game.min.js"))
    .pipe(gulp.dest('build/js'));
});

gulp.task('build:deploy', ['compress', 'clean', 'compile','build'], function(cb) {
  del('build/js/game.js', cb);
  gulp.src('web/game-deploy.html')
    .pipe(rename("game.html"))
    .pipe(gulp.dest('build'));
});

gulp.task('default', ['build:deploy']);
